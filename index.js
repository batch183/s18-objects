console.log("Hello World");

let trainer = {
	name: "Ash Ketchum",
	age: 10,
	friends: {
		hoenn: ["May", "Max"],
		kanto: ["Brock", "Misty"]
	},
	pokemon: ["Pikachu", "Charizerd", "Squirtle", "Bulbasaur"],
	talk: function(){
		console.log(trainer.pokemon[0]+"! I choose you!");
	}
}

console.log(trainer);
console.log("Result of dot notation:");
console.log(trainer.name);
console.log("Result of square notation:");
console.log(trainer["pokemon"]);
console.log("Result of talk method:");
console.log(trainer.talk());

// Object Constructor Notation
function Pokemon(name, level){
	// Propeties
	this.name = name;
	this.level = level;
	this.health = level * 2;
	this.attack = level;

	// Methods			 //targetPokemon is object data type
	this.tackle = function(target){
				  //pokemonObject		//targetPokemon
		console.log(this.name +" tackled "+ target.name);
		target.health = target.health - this.attack
		// 24 - 8 = 16
		// call faint method if the target pokemon's health is less than or equal to zero
		if(target.health<=0){
			console.log(target.name+"'s health is reduced to "+ target.health + ".");
			target.faint();
		}
		else {
			console.log(target.name+"'s health is reduced to "+ target.health + ".");
		}

	}
	this.faint = function(){
		console.log(this.name+ " fainted.");
	}
}

let pikachu = new Pokemon("Pikachu", 12);
let geodude = new Pokemon("Geodude", 8);
let mewtwo = new Pokemon("Mewtwo",100);
/*

	health = 24;
	attack = 12;

*/

console.log(pikachu);
console.log(geodude);
console.log(mewtwo);
geodude.tackle(pikachu);
console.log(pikachu);
mewtwo.tackle(geodude);
console.log(geodude);